import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:csv/csv.dart' as csv;
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;


class Alumno {
  final int semestre;
  final String grupo;
  final int matricula;
  final String nombre;
  final String clavecita;

  Alumno({
    required this.semestre,
    required this.grupo,
    required this.matricula,
    required this.nombre,
    required this.clavecita,
  });
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Alumno> alumnos = [];

  @override
  void initState() {
    super.initState();
    loadAlumnos();
  }

  Future<void> loadAlumnos() async {
    final jsonData = await rootBundle.loadString('assets/estu.json');
    final List<dynamic> jsonList = json.decode(jsonData);

    for (final jsonAlumno in jsonList) {
      final alumno = Alumno(
        semestre: jsonAlumno['Semestre'],
        grupo: jsonAlumno['Grupo'],
        matricula: jsonAlumno['Matricula'],
        nombre: jsonAlumno['Nombre'],
        clavecita: jsonAlumno['clavecita'],
      );
      alumnos.add(alumno);
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final int searchKey =42302460;
    Alumno? resultRow;

    if (alumnos.isNotEmpty) {
      for (final row in alumnos) {
        if (row.matricula==searchKey) {
          resultRow = row;
          break;
        }
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Buscar en CSV'),
      ),
      body: Center(
        child: resultRow != null
            ? Card(
                child: Column(
                  children: [
                    ListTile(
                      title: Text('Nombre: ${resultRow.nombre}'),
                      subtitle: Text('Clavecita: ${resultRow.clavecita}'),
                    ),
                  ],
                ),
              )
            : Text('No se encontró la clave "$searchKey" en el archivo CSV.'),
      ),
    );
  }
}