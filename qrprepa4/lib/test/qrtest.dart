import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class QRScannerPage extends StatefulWidget {
  @override
  _QRScannerPageState createState() => _QRScannerPageState();
}

class _QRScannerPageState extends State<QRScannerPage> {
  String qrCodeResult = "Aún no se ha escaneado ningún código QR";

  Future<void> scanQRCode() async {
    String scanResult = await FlutterBarcodeScanner.scanBarcode(
      "#ff6666", // Color de fondo del escáner
      "Cancelar", // Texto del botón de cancelar
      true, // Usar la cámara trasera (false para la cámara frontal)
      ScanMode.QR, // Modo de escaneo (QR, AZTEC, o otros)
    );

    if (!mounted) return;

    if (scanResult == "-1") {
      // El usuario canceló el escaneo
      setState(() {
        qrCodeResult = "Escaneo cancelado";
      });
    } else {
      // Se detectó un código QR
      setState(() {
        qrCodeResult = "Resultado del escaneo: $scanResult";
      });

      // Escaneo exitoso, muestra el resultado en un pop-up
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Resultado del escaneo"),
            content: Text("Resultado: $scanResult"),
            actions: [
              TextButton(onPressed:(){
                Navigator.pop(context);
                scanQRCode();
              }, child: Text('Scanea otro codigo')),
              TextButton(

                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Cerrar"),
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QR Scanner Example'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              qrCodeResult,
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: scanQRCode,
              child: Text('Escanear QR Code'),
            ),
          ],
        ),
      ),
    );
  }
}