import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class QRCodeScanner {
  String qrCodeResult = "Aún no se ha escaneado ningún código";

  Future<void> scanQRCode(BuildContext context) async {
    String scanResult = await FlutterBarcodeScanner.scanBarcode(
      "#ff6666",
      "Cancelar",
      true,
      ScanMode.QR,
    );

    if (scanResult == "-1") {
      // El usuario canceló el escaneo
      qrCodeResult = "Escaneo cancelado";
    } else {
      // Se detectó un código QR
      qrCodeResult = "Resultado del escaneo: $scanResult";

      // Escaneo exitoso, muestra el resultado en un pop-up
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Resultado del escaneo"),
            content: Text("Resultado: $scanResult"),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  scanQRCode(context); // Redirige al escáner para escanear otro código
                },
                child: Text('Escanear otro código'),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Cerrar"),
              ),
            ],
          );
        },
      );
    }
  }
}