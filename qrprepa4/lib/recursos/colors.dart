import 'package:flutter/material.dart';


class ColorsPersonal{
  static final  letters = Colors.white;
  static final backGround = Color.fromARGB(228, 14, 42, 53);
  static final backGroundSucces = Color.fromARGB(179, 2, 238, 128);
  static final backGroundError = Color.fromARGB(179, 189, 22, 22);
  static final backGroundUaz = Color.fromARGB(255, 240, 226, 27);
  static final backGroundCard = Color.fromARGB(255, 23, 13, 151);
}
