/**
 * Clase especial para almacenar
 * datos de tipo string para 
 * titulos y textos
 */

class PantallaString{
  //STRING ENCARGADOS DE TITULOS
  static final titulo = 'Registro de Acccesos y Salidas';
  static final tituloAcceso = 'Registro de accesos';
  static final tituloSalidas= 'Registro de Salidas';
  static final tituloMenuOpciones='Menu Opciones';
  static final tituloReporte = 'Descarga CSV y Genera Reporte';
  static final tituloCardReporte = 'Descargar CSV';
  static final tituloCardInfoAlumnoEspera = 'Informacion del Alumno';

  //STRINGS ERRORS
  static final errorAlumNotFound = 'No se encontro Alumno';

  //STRINGS ENCARGADOS DE DESCRIPCIONES DE CARTAS
  static final descAccCard='Leer los alumnos para acceder al plantel, se registraran en la base de datos';
  static final descSalCard='Leer los alumnos para salir al plantel, se registraran en la base de datos';
  static final descReportCard = 'Descarga el CSV de los datos almacenados';

  //STRING PARA BOTONES
  static final btnRegistrar = 'Registrar';
  static final btnGenReporte='Generar Reporte';
  static final btnDescargaCSV ='Descarga CSV';

  //STRING DE AYUDAS
  static final errFaltaScan = 'Aun no se ha escaneado ningun codigo';

  //STRINGS LABELS DE ALERDIALOG
  static final nombreLbl = 'Nombre: ';
  static final matriculaLbl = 'Matricula: ';
  static final gradoLbl = 'Grado: ';
  static final horaLbl = 'Hora: ';
  static final fechaIngresoLbl = 'Fecha Ingreso: ';
  static final fechaSalidaLbl = 'Fecha Salida: ';
  static final movimientoLbl = 'Movimiento: ';
}
